package com.zdt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author zdt
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class ZdtApplication
{
    public static void main(String[] args)
    {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(ZdtApplication.class, args);
        System.out.println("             ██   ██             ██                                      \n" +
                "            ░██  ░██            ░██               ██████                 \n" +
                " ██████     ░██ ██████    ██████░██       ██████ ░██░░░██  █████   █████ \n" +
                "░░░░██   ██████░░░██░    ██░░░░ ░██████  ██░░░░██░██  ░██ ██░░░██ ██░░░██\n" +
                "   ██   ██░░░██  ░██    ░░█████ ░██░░░██░██   ░██░██████ ░███████░███████\n" +
                "  ██   ░██  ░██  ░██     ░░░░░██░██  ░██░██   ░██░██░░░  ░██░░░░ ░██░░░░ \n" +
                " ██████░░██████  ░░██    ██████ ░██  ░██░░██████ ░██     ░░██████░░██████\n" +
                "░░░░░░  ░░░░░░    ░░    ░░░░░░  ░░   ░░  ░░░░░░  ░░       ░░░░░░  ░░░░░░ \n");
    }
}
