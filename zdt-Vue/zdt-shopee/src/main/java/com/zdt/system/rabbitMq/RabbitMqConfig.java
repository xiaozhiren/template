package com.zdt.system.rabbitMq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息队列配置
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 消息实际消费队列所绑定的交换机
     */
    @Bean
    DirectExchange Direct() {
        return (DirectExchange) ExchangeBuilder
                .directExchange(QueueEnum.QUEUE_CANCEL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 延迟队列队列所绑定的交换机
     */
    @Bean
    DirectExchange TtlDirect() {
        return (DirectExchange) ExchangeBuilder
                .directExchange(QueueEnum.QUEUE_TTL_CANCEL.getExchange())
                .durable(true)
                .build();
    }

    /**
     * 实际消费队列
     */
    @Bean
    public Queue Queue() {
        return new Queue(QueueEnum.QUEUE_CANCEL.getName());
    }

    /**
     * 延迟队列（死信队列）
     */
    @Bean
    public Queue TtlQueue() {
        return QueueBuilder
                .durable(QueueEnum.QUEUE_TTL_CANCEL.getName())
                .withArgument("x-dead-letter-exchange", QueueEnum.QUEUE_CANCEL.getExchange())//到期后转发的交换机
                .withArgument("x-dead-letter-routing-key", QueueEnum.QUEUE_CANCEL.getRouteKey())//到期后转发的路由键
                .build();
    }

    /**
     * 将队列绑定到交换机
     */
    @Bean
    Binding Binding(DirectExchange Direct,Queue Queue){
        return BindingBuilder
                .bind(Queue)
                .to(Direct)
                .with(QueueEnum.QUEUE_CANCEL.getRouteKey());
    }

    /**
     * 将延迟队列绑定到交换机
     */
    @Bean
    Binding TtlBinding(DirectExchange TtlDirect,Queue TtlQueue){
        return BindingBuilder
                .bind(TtlQueue)
                .to(TtlDirect)
                .with(QueueEnum.QUEUE_TTL_CANCEL.getRouteKey());
    }

}
