package com.zdt.system.socket;

import com.alibaba.fastjson.JSONObject;
import com.zdt.common.utils.uuid.UUID;

/**
 * 通知返回类
 */
public class Notice {
    /**
     * id
     */
    private String id;
    /**
     * 返回类型
     */
    private NoticeType noticeType;

    /**
     * 返回内容
     */
    private String msg;

    /**
     * 是否声音提醒
     */
    private Boolean diDong;

    /**
     * 接收人
     */
    private String userName;

    /**
     * 是否已读
     */
    private Boolean read;

    public Notice(String id, NoticeType noticeType, String msg, Boolean diDong, String userName, Boolean read) {
        this.id = id;
        this.noticeType = noticeType;
        this.msg = msg;
        this.diDong = diDong;
        this.userName = userName;
        this.read = read;
    }

    /**
     * 构建放回对象
     * @param noticeType    返回类型
     * @param msg           返回内容
     * @param userName      发送给此用户
     * @param diDong        是否声音提醒(长度为1)
     */
    public Notice(NoticeType noticeType, String msg, String userName, Boolean ... diDong ){
        this.noticeType = noticeType;
        this.msg = msg;
        this.userName = userName;
        this.diDong = diDong.length > 0 ? diDong[0] : false;
        this.id = UUID.fastUUID().toString();
        this.read = Boolean.FALSE;
    }
    public Notice(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NoticeType getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(NoticeType noticeType) {
        this.noticeType = noticeType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getDiDong() {
        return diDong;
    }

    public void setDiDong(Boolean diDong) {
        this.diDong = diDong;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    @Override
    public String toString() {
        JSONObject json = (JSONObject) JSONObject.toJSON(this);
        return json.toString();
    }
}

