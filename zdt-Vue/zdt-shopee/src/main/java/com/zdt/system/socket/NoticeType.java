package com.zdt.system.socket;

/**
 * 通知类型枚举状态类
 */
public enum NoticeType {
    /**
     * 弱提醒
     */
    MESSAGE("Message"),
    /**
     * 必须确认读取才会关闭,否则一直提醒(弹窗)
     */
    MESSAGEBOX ("MessageBox "),
    /**
     * 需手动关闭的通知
     */
    NOTIFICATION("Notification");


    private String name;

    NoticeType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
