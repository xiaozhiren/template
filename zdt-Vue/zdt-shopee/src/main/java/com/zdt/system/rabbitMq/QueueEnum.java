package com.zdt.system.rabbitMq;

/**
 * 消息队列枚举配置
 */
public enum QueueEnum {
    /**
     * 消息通知队列
     */
    QUEUE_CANCEL("zdt.direct", "zdt.cancel", "zdt.cancel"),
    /**
     * 消息通知ttl队列
     */
    QUEUE_TTL_CANCEL("zdt.direct.ttl", "zdt.cancel.ttl", "zdt.cancel.ttl");

    /**
     * 交换名称
     */
    private String exchange;
    /**
     * 队列名称
     */
    private String name;
    /**
     * 路由键
     */
    private String routeKey;

    QueueEnum(String exchange, String name, String routeKey) {
        this.exchange = exchange;
        this.name = name;
        this.routeKey = routeKey;
    }

    public String getExchange() {
        return exchange;
    }

    public String getName() {
        return name;
    }

    public String getRouteKey() {
        return routeKey;
    }
}
