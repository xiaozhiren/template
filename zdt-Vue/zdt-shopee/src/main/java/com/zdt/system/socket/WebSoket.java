package com.zdt.system.socket;

import com.zdt.common.constant.Constants;
import com.zdt.common.core.domain.model.LoginUser;
import com.zdt.common.core.redis.RedisCache;
import com.zdt.common.utils.StringUtils;
import com.zdt.common.utils.spring.SpringUtils;
import com.zdt.framework.web.service.TokenService;
import io.jsonwebtoken.Claims;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@ServerEndpoint(value = "/websocket/{Authorization}")
@Component
public class WebSoket {

    //与每个用户的链接会话
    public static HashMap<String, List<Session>> userSession = new HashMap<String, List<Session>>();

    private static TokenService tokenService = SpringUtils.getBean(TokenService.class);;

    private static RedisCache redisCache = SpringUtils.getBean(RedisCache.class);
    /**
     * 存的通知,未被用户读取的通知
     */
    private static  List<Notice> notices = new ArrayList<>();

    /**
     * 发送失败的通知,可能是因为用户不在线
     */
    private static  List<Notice> errorNotices = new ArrayList<>();

    /**
     * 连接建立成功调用的方法
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("Authorization")String  authorization) {
        String userName = getUserName(authorization);
        if (userName != null) {
            List<Session> sessions = userSession.get(userName);
            if(sessions != null && sessions.size()>0){
                userSession.get(userName).add(session);
            }else{
                ArrayList<Session> sessionList = new ArrayList<Session>();
                sessionList.add(session);
                userSession.put(userName, sessionList);
            }
            //被关闭的资源数大于0,       删除已经被关闭的资源
            if(userSession.get(userName).stream().filter(v-> v == null || !v.isOpen()).count() > 0){
                userSession.put(userName, userSession.get(userName).stream().filter(v-> v != null && v.isOpen()).collect(Collectors.toList()));
            }
            //发送失败的信息继续发送
            for(Notice notice : notices){
                if(userName.equals(notice.getUserName())){
                    sendMessage(notice);
                }
            }
            for(Notice notice : errorNotices){
                if(userName.equals(notice.getUserName())){
                    sendMessage(notice);
                }
            }
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session, @PathParam("Authorization")String  authorization) {
        String userName = getUserName(authorization);
        if (userName != null) {
            List<Session> sessions = userSession.get(userName);
            if(sessions != null && sessions.size()>0){
                userSession.get(userName).removeIf(s -> s.getId().contains(session.getId()));
            }
        }
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 消息内容
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        //移除已读的信息
        JSONObject jsonObject= JSONObject.fromObject(message);
        Notice notice = (Notice)JSONObject.toBean(jsonObject, Notice.class);
        if(notice.getRead() && NoticeType.MESSAGEBOX == notice.getNoticeType()){
            for (Notice data : notices) {
                if(data.getId().equals(notice.getId())){
                    notices.remove(data);
                    break;
                }
            }
            notices.stream().filter(v -> v.getId().equals(notice.getRead())).collect(Collectors.toList());
        }
    }

    /**
     * 连接发生错误时的调用方法
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {

    }


    /**
     * 简单数据发送,发送失败就是失败,不会二次发送
     * @param userName      用户名
     * @param message       消息内容
     * @return              是否发送成功
     */
    public static Boolean sendMessage(String userName, String message) {
        AtomicReference<Boolean> bool = new AtomicReference<>(false);
        userSession.entrySet().stream().forEach((entiy)->{
            if(entiy.getKey().equals(userName)){
                List<Session> sessions = entiy.getValue();
                sessions.stream().filter((v)-> v.isOpen()).forEach(v->{
                    try {
                        bool.set(true);
                        v.getBasicRemote().sendText(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }

        });
        return bool.get();
    }

    /**
     * 发送数据给用户,如果发送失败,下次用户上线会再次发送,MESSAGEBOX的信息必须已读才会视为发送成功
     * @param notice        返回信息对象
     * @return              是否发送成功
     */
    public static Boolean sendMessage(Notice notice) {
        AtomicReference<Boolean> bool = new AtomicReference<>(false);
        try {
            userSession.entrySet().stream().forEach((entiy)->{
                if(entiy.getKey().equals(notice.getUserName())){
                    List<Session> sessions = entiy.getValue();
                    sessions.stream().filter((v)-> v.isOpen()).forEach(v->{
                        try {
                            v.getBasicRemote().sendText(notice.toString());
                            bool.set(true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            });
        } catch (Exception e) {
        }
        //如果是必须读取才会删除的通知
        if(NoticeType.MESSAGEBOX.equals(notice.getNoticeType()) && bool.get()){
            Boolean distinct = false;
            for(Notice notice1 : notices){
                if(notice.getId() == notice1.getId()){
                    distinct = true;
                }
            }
            if(!distinct){
                notices.add(notice);
            }

        }else if(!bool.get()){
            Boolean distinct = false;
            for(Notice notice1 : errorNotices){
                if(notice.getId() == notice1.getId()){
                    distinct = true;
                }
            }
            if(!distinct){
                errorNotices.add(notice);
            }
        }
        return bool.get();
    }


    /**
     * 根据token权限标识获取用户名
     * @param authorization
     * @return
     */
    public String getUserName(String authorization){
        String userName = null;
        if (StringUtils.isNotEmpty(authorization))
        {
            Claims claims = tokenService.parseToken(authorization);
            // 解析对应的权限以及用户信息
            String uuid = (String) claims.get(Constants.LOGIN_USER_KEY);
            String userKey = tokenService.getTokenKey(uuid);
            LoginUser user = redisCache.getCacheObject(userKey);
            if(user != null){
                userName = user.getUsername();
            }
        }
        return userName;
    }

}
