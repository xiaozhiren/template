package com.zdt.order.mapper;

import java.util.List;
import com.zdt.order.domain.OeOrderInfoShopee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 订单信息Mapper接口
 * 
 * @author zdt
 * @date 2021-05-10
 */
@Component("OeOrderInfoShopeeMapper")
public interface OeOrderInfoShopeeMapper 
{
    /**
     * 查询订单信息
     * 
     * @param oeOrderInfoShopeeId 订单信息ID
     * @return 订单信息
     */
    public OeOrderInfoShopee selectOeOrderInfoShopeeById(Long oeOrderInfoShopeeId);

    /**
     * 查询订单信息列表
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 订单信息集合
     */
    public List<OeOrderInfoShopee> selectOeOrderInfoShopeeList(OeOrderInfoShopee oeOrderInfoShopee);

    /**
     * 新增订单信息
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 结果
     */
    public int insertOeOrderInfoShopee(OeOrderInfoShopee oeOrderInfoShopee);

    /**
     * 批量新增订单信息
     *
     * @param oeOrderInfoShopees 订单信息
     * @return 结果
     */
    public int insertOeOrderInfoShopees(@Param("oeOrderInfoShopees")List<OeOrderInfoShopee> oeOrderInfoShopees);

    /**
     * 修改订单信息
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 结果
     */
    public int updateOeOrderInfoShopee(OeOrderInfoShopee oeOrderInfoShopee);

    /**
     * 删除订单信息
     * 
     * @param oeOrderInfoShopeeId 订单信息ID
     * @return 结果
     */
    public int deleteOeOrderInfoShopeeById(Long oeOrderInfoShopeeId);

    /**
     * 批量删除订单信息
     * 
     * @param oeOrderInfoShopeeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteOeOrderInfoShopeeByIds(Long[] oeOrderInfoShopeeIds);
}
