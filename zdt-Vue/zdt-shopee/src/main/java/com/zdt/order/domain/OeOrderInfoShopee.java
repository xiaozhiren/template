package com.zdt.order.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zdt.common.annotation.Excel;
import com.zdt.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 订单信息对象 oe_order_info_shopee
 * 
 * @author zdt
 * @date 2021-05-10
 */
public class OeOrderInfoShopee  // extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ExcelIgnore
    private Long oeOrderInfoShopeeId;

    /** 订单Id（中达通定义6-8位 ） */
    @Excel(name = "订单Id", readConverterExp = "中达通定义6-8位")
    @ExcelProperty("订单Id（中达通定义6-8位 ）")
    private String sysOrderId;

    /** 客户订单编号 */
    @Excel(name = "客户订单编号")
    @ExcelProperty("客户订单编号")
    private String orderCode;

    /** 虾皮上传pdf文件url */
    @Excel(name = "虾皮上传pdf文件url")
    @ExcelIgnore
//    @ExcelProperty("字符串标题")
    private String shopeeFileUrl;

    /** SKU编码 */
    @Excel(name = "SKU编码")
    @ExcelProperty("SKU编码")
    private String skuCode;

    /** 仓库状态(申请发货1， 已发货2) */
    @Excel(name = "仓库状态(申请发货1， 已发货2)")
    @ExcelIgnore
//    @ExcelProperty("字符串标题")
    private String warehouseStatus;

    /** 备注 */
    @Excel(name = "备注")
    @ExcelProperty("备注")
    private String orderRemarks;

    /** 件数 */
    @Excel(name = "件数")
    @ExcelProperty("件数")
    private Long goodsNumber;

    /** 目的地 */
    @Excel(name = "目的地")
    @ExcelProperty("目的地")
    private String destination;

    /** 重量 */
    @Excel(name = "重量")
    @ExcelProperty("重量")
    private BigDecimal weight;

    /** 收件人 */
    @Excel(name = "收件人")
    @ExcelProperty("收件人")
    private String recipientName;

    /** 收件地址 */
    @Excel(name = "收件地址")
    @ExcelProperty("收件地址")
    private String shippingAddress;

    /** 收件电话 */
    @Excel(name = "收件电话")
    @ExcelProperty("收件电话")
    private String recipientPhone;

    /** 邮箱 */
    @Excel(name = "邮箱")
    @ExcelProperty("邮箱")
    private String email;

    /** 手机 */
    @Excel(name = "手机")
    @ExcelProperty("手机")
    private String mobile;

    /** 收件邮编 */
    @Excel(name = "收件邮编")
    @ExcelProperty("收件邮编")
    private String postcode;

    /** 收件国家 */
    @Excel(name = "收件国家")
    @ExcelProperty("收件国家")
    private String country;

    /** 收件省州 */
    @Excel(name = "收件省州")
    @ExcelProperty("收件省州")
    private String provinceState;

    /** 收件城市 */
    @Excel(name = "收件城市")
    @ExcelProperty("收件城市")
    private String city;

    /** 中文品名 */
    @Excel(name = "中文品名")
    @ExcelProperty("中文品名")
    private String productName;

    /** 英文品名 */
    @Excel(name = "英文品名")
    @ExcelProperty("英文品名")
    private String englishProductName;

    /** 数量 */
    @Excel(name = "数量")
    @ExcelProperty("数量")
    private Long quantity;

    /** 单价 */
    @Excel(name = "单价")
    @ExcelProperty("单价")
    private BigDecimal unitPrice;

    /** 申报 */
    @Excel(name = "申报")
    @ExcelProperty("申报")
    private String declares;

    /** 配货信息 */
    @Excel(name = "配货信息")
    @ExcelProperty("配货信息")
    private String distributionInformation;

    /** $column.columnComment */
    @Excel(name = "配货信息")
    @ExcelIgnore
    private Long lastUpdatedBy;

    /** $column.columnComment */
    @Excel(name = "配货信息")
    @ExcelIgnore
    private Long createOrgBy;

    /** $column.columnComment */
    @Excel(name = "配货信息")
    @ExcelIgnore
    private String createOrg;

    /** 创建者 */
    @Excel(name = "创建者")
    @ExcelIgnore
    private Long createBy;

    @Excel(name = "创建时间")
    @ExcelIgnore
    private Date createTime;

    @Excel(name = "修改时间")
    @ExcelIgnore
    private Date updateTime;

//    /** 修改者 */
//    @Excel(name = "修改者")
//    private Long updateBy;



    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setOeOrderInfoShopeeId(Long oeOrderInfoShopeeId)
    {
        this.oeOrderInfoShopeeId = oeOrderInfoShopeeId;
    }

    public Long getOeOrderInfoShopeeId() 
    {
        return oeOrderInfoShopeeId;
    }
    public void setSysOrderId(String sysOrderId) 
    {
        this.sysOrderId = sysOrderId;
    }

    public String getSysOrderId() 
    {
        return sysOrderId;
    }
    public void setOrderCode(String orderCode) 
    {
        this.orderCode = orderCode;
    }

    public String getOrderCode()

    {
        return orderCode;
    }
    public void setShopeeFileUrl(String shopeeFileUrl) 
    {
        this.shopeeFileUrl = shopeeFileUrl;
    }

    public String getShopeeFileUrl() 
    {
        return shopeeFileUrl;
    }
    public void setSkuCode(String skuCode) 
    {
        this.skuCode = skuCode;
    }

    public String getSkuCode() 
    {
        return skuCode;
    }
    public void setWarehouseStatus(String warehouseStatus) 
    {
        this.warehouseStatus = warehouseStatus;
    }

    public String getWarehouseStatus() 
    {
        return warehouseStatus;
    }
    public void setOrderRemarks(String orderRemarks) 
    {
        this.orderRemarks = orderRemarks;
    }

    public String getOrderRemarks() 
    {
        return orderRemarks;
    }
    public void setGoodsNumber(Long goodsNumber) 
    {
        this.goodsNumber = goodsNumber;
    }

    public Long getGoodsNumber() 
    {
        return goodsNumber;
    }
    public void setDestination(String destination) 
    {
        this.destination = destination;
    }

    public String getDestination() 
    {
        return destination;
    }
    public void setWeight(BigDecimal weight) 
    {
        this.weight = weight;
    }

    public BigDecimal getWeight() 
    {
        return weight;
    }
    public void setRecipientName(String recipientName) 
    {
        this.recipientName = recipientName;
    }

    public String getRecipientName() 
    {
        return recipientName;
    }
    public void setShippingAddress(String shippingAddress) 
    {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingAddress() 
    {
        return shippingAddress;
    }
    public void setRecipientPhone(String recipientPhone) 
    {
        this.recipientPhone = recipientPhone;
    }

    public String getRecipientPhone() 
    {
        return recipientPhone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setPostcode(String postcode) 
    {
        this.postcode = postcode;
    }

    public String getPostcode() 
    {
        return postcode;
    }
    public void setCountry(String country) 
    {
        this.country = country;
    }

    public String getCountry() 
    {
        return country;
    }
    public void setProvinceState(String provinceState) 
    {
        this.provinceState = provinceState;
    }

    public String getProvinceState() 
    {
        return provinceState;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setEnglishProductName(String englishProductName) 
    {
        this.englishProductName = englishProductName;
    }

    public String getEnglishProductName() 
    {
        return englishProductName;
    }
    public void setQuantity(Long quantity) 
    {
        this.quantity = quantity;
    }

    public Long getQuantity() 
    {
        return quantity;
    }
    public void setUnitPrice(BigDecimal unitPrice) 
    {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getUnitPrice() 
    {
        return unitPrice;
    }
    public void setDeclares(String declares) 
    {
        this.declares = declares;
    }

    public String getDeclares() 
    {
        return declares;
    }
    public void setDistributionInformation(String distributionInformation) 
    {
        this.distributionInformation = distributionInformation;
    }

    public String getDistributionInformation() 
    {
        return distributionInformation;
    }
    public void setLastUpdatedBy(Long lastUpdatedBy) 
    {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Long getLastUpdatedBy() 
    {
        return lastUpdatedBy;
    }
    public void setCreateOrgBy(Long createOrgBy) 
    {
        this.createOrgBy = createOrgBy;
    }

    public Long getCreateOrgBy() 
    {
        return createOrgBy;
    }
    public void setCreateOrg(String createOrg) 
    {
        this.createOrg = createOrg;
    }

    public String getCreateOrg() 
    {
        return createOrg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("oeOrderInfoShopeeId", getOeOrderInfoShopeeId())
            .append("sysOrderId", getSysOrderId())
            .append("orderCode", getOrderCode())
            .append("shopeeFileUrl", getShopeeFileUrl())
            .append("skuCode", getSkuCode())
            .append("warehouseStatus", getWarehouseStatus())
            .append("orderRemarks", getOrderRemarks())
            .append("goodsNumber", getGoodsNumber())
            .append("destination", getDestination())
            .append("weight", getWeight())
            .append("recipientName", getRecipientName())
            .append("shippingAddress", getShippingAddress())
            .append("recipientPhone", getRecipientPhone())
            .append("email", getEmail())
            .append("mobile", getMobile())
            .append("postcode", getPostcode())
            .append("country", getCountry())
            .append("provinceState", getProvinceState())
            .append("city", getCity())
            .append("productName", getProductName())
            .append("englishProductName", getEnglishProductName())
            .append("quantity", getQuantity())
            .append("unitPrice", getUnitPrice())
            .append("declares", getDeclares())
            .append("distributionInformation", getDistributionInformation())
            .append("createBy", getCreateBy())
            .append("lastUpdatedBy", getLastUpdatedBy())
            .append("createTime", getCreateTime())
            .append("createOrgBy", getCreateOrgBy())
            .append("createOrg", getCreateOrg())
            .toString();
    }
}
