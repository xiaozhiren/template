package com.zdt.order.controller;

import java.util.List;

import com.zdt.common.annotation.Log;
import com.zdt.common.core.controller.BaseController;
import com.zdt.common.core.domain.AjaxResult;
import com.zdt.common.core.page.TableDataInfo;
import com.zdt.common.enums.BusinessType;
import com.zdt.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zdt.order.domain.OeOrderInfoShopee;
import com.zdt.order.service.IOeOrderInfoShopeeService;


/**
 * 订单信息Controller
 * 
 * @author zdt
 * @date 2021-05-10
 */
@RestController
@RequestMapping("/shopee/shopeeOrder")
public class OeOrderInfoShopeeController extends BaseController
{
    @Autowired
    private IOeOrderInfoShopeeService oeOrderInfoShopeeService;

    /**
     * 查询订单信息列表
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(OeOrderInfoShopee oeOrderInfoShopee)
    {
        startPage();
        List<OeOrderInfoShopee> list = oeOrderInfoShopeeService.selectOeOrderInfoShopeeList(oeOrderInfoShopee);
        return getDataTable(list);
    }

    /**
     * 导出订单信息列表
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:export')")
    @Log(title = "订单信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OeOrderInfoShopee oeOrderInfoShopee)
    {
        List<OeOrderInfoShopee> list = oeOrderInfoShopeeService.selectOeOrderInfoShopeeList(oeOrderInfoShopee);
        ExcelUtil<OeOrderInfoShopee> util = new ExcelUtil<OeOrderInfoShopee>(OeOrderInfoShopee.class);
        return util.exportExcel(list, "订单信息数据");
    }

    /**
     * 获取订单信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:query')")
    @GetMapping(value = "/{oeOrderInfoShopeeId}")
    public AjaxResult getInfo(@PathVariable("oeOrderInfoShopeeId") Long oeOrderInfoShopeeId)
    {
        return AjaxResult.success(oeOrderInfoShopeeService.selectOeOrderInfoShopeeById(oeOrderInfoShopeeId));
    }

    /**
     * 新增订单信息
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:add')")
    @Log(title = "订单信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OeOrderInfoShopee oeOrderInfoShopee)
    {
        return toAjax(oeOrderInfoShopeeService.insertOeOrderInfoShopee(oeOrderInfoShopee));
    }

    /**
     * 修改订单信息
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:edit')")
    @Log(title = "订单信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OeOrderInfoShopee oeOrderInfoShopee)
    {
        return toAjax(oeOrderInfoShopeeService.updateOeOrderInfoShopee(oeOrderInfoShopee));
    }

    /**
     * 删除订单信息
     */
    @PreAuthorize("@ss.hasPermi('shopee:shopeeOrder:remove')")
    @Log(title = "订单信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{oeOrderInfoShopeeIds}")
    public AjaxResult remove(@PathVariable Long[] oeOrderInfoShopeeIds)
    {
        return toAjax(oeOrderInfoShopeeService.deleteOeOrderInfoShopeeByIds(oeOrderInfoShopeeIds));
    }
}
