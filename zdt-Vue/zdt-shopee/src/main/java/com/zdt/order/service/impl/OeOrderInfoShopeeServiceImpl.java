package com.zdt.order.service.impl;

import java.util.List;

import com.zdt.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zdt.order.mapper.OeOrderInfoShopeeMapper;
import com.zdt.order.domain.OeOrderInfoShopee;
import com.zdt.order.service.IOeOrderInfoShopeeService;

/**
 * 订单信息Service业务层处理
 * 
 * @author zdt
 * @date 2021-05-10
 */
@Service
public class OeOrderInfoShopeeServiceImpl implements IOeOrderInfoShopeeService 
{
    @Autowired
    private OeOrderInfoShopeeMapper oeOrderInfoShopeeMapper;

    /**
     * 查询订单信息
     * 
     * @param oeOrderInfoShopeeId 订单信息ID
     * @return 订单信息
     */
    @Override
    public OeOrderInfoShopee selectOeOrderInfoShopeeById(Long oeOrderInfoShopeeId)
    {
        return oeOrderInfoShopeeMapper.selectOeOrderInfoShopeeById(oeOrderInfoShopeeId);
    }

    /**
     * 查询订单信息列表
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 订单信息
     */
    @Override
    public List<OeOrderInfoShopee> selectOeOrderInfoShopeeList(OeOrderInfoShopee oeOrderInfoShopee)
    {
        return oeOrderInfoShopeeMapper.selectOeOrderInfoShopeeList(oeOrderInfoShopee);
    }

    /**
     * 新增订单信息
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 结果
     */
    @Override
    public int insertOeOrderInfoShopee(OeOrderInfoShopee oeOrderInfoShopee)
    {
        oeOrderInfoShopee.setCreateTime(DateUtils.getNowDate());
        return oeOrderInfoShopeeMapper.insertOeOrderInfoShopee(oeOrderInfoShopee);
    }

    /**
     * 修改订单信息
     * 
     * @param oeOrderInfoShopee 订单信息
     * @return 结果
     */
    @Override
    public int updateOeOrderInfoShopee(OeOrderInfoShopee oeOrderInfoShopee)
    {
        oeOrderInfoShopee.setUpdateTime(DateUtils.getNowDate());
        return oeOrderInfoShopeeMapper.updateOeOrderInfoShopee(oeOrderInfoShopee);
    }

    /**
     * 批量删除订单信息
     * 
     * @param oeOrderInfoShopeeIds 需要删除的订单信息ID
     * @return 结果
     */
    @Override
    public int deleteOeOrderInfoShopeeByIds(Long[] oeOrderInfoShopeeIds)
    {
        return oeOrderInfoShopeeMapper.deleteOeOrderInfoShopeeByIds(oeOrderInfoShopeeIds);
    }

    /**
     * 删除订单信息信息
     * 
     * @param oeOrderInfoShopeeId 订单信息ID
     * @return 结果
     */
    @Override
    public int deleteOeOrderInfoShopeeById(Long oeOrderInfoShopeeId)
    {
        return oeOrderInfoShopeeMapper.deleteOeOrderInfoShopeeById(oeOrderInfoShopeeId);
    }
}
