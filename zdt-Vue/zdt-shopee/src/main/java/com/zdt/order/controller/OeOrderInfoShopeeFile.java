package com.zdt.order.controller;

import com.alibaba.excel.EasyExcel;
import com.zdt.common.core.controller.BaseController;
import com.zdt.common.core.domain.AjaxResult;
import com.zdt.common.utils.file.FileUtils;
import com.zdt.common.utils.uuid.IdUtils;
import com.zdt.order.domain.OeOrderInfoShopee;
import com.zdt.order.mapper.OeOrderInfoShopeeMapper;
import com.zdt.order.service.IOeOrderInfoShopeeListener;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 订单信息Controller
 *
 * @author zdt
 * @date 2021-05-08
 */
@RestController
public class OeOrderInfoShopeeFile
{
    @Value("${path.windows}")
    private String winPath;
    @Value("${path.luinx}")
    private String luinxPath;
    @Autowired
    private OeOrderInfoShopeeMapper oeOrderInfoShopeeMapper;
    /**
     * <p>Description: 虾皮订单上传</p>
     * @param file    上传附件
     * @return
     */
    @PostMapping("/upload/OrderInfoShopee")
    public AjaxResult uploadTemplate(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        if (StringUtils.isBlank(fileName)) {
            return AjaxResult.error("文件不能为空,");
        }
        long fileSize = file.getSize();
        if (fileSize > 10 * 1024 * 1024) {
            return AjaxResult.error("上传文件大小超出限定大小10M,可以尝试分多次上传,");
        }
        try {
            // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
            EasyExcel.read(file.getInputStream(), OeOrderInfoShopee.class, new IOeOrderInfoShopeeListener(oeOrderInfoShopeeMapper)).sheet().doRead();
        }catch (Exception ex){
            return AjaxResult.error(ex.toString());
        }
        // 获取配置路径
        String path;
        if(System.getProperty("os.name").toLowerCase().indexOf("windows")>=0){
            path = winPath;
        }else{
            path = luinxPath;
        }
        String newPath = path + new SimpleDateFormat("yyyy/MM/dd/").format(new Date()) + IdUtils.simpleUUID().replaceAll("-", "") + "/";
        File newDir = new File(newPath);
        if (!newDir.exists()) {
            newDir.mkdirs(); // 目录不存在的情况下，创建目录
        }
        File newFile = new File(newDir, fileName);
        //通过CommonsMultipartFile的方法直接写文件
        file.transferTo(newFile);
        return AjaxResult.success("导入并识别成功");
    }

}
