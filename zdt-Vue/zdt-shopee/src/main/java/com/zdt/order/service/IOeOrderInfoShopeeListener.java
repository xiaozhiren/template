package com.zdt.order.service;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zdt.common.core.domain.model.LoginUser;
import com.zdt.common.utils.SecurityUtils;
import com.zdt.common.utils.ServletUtils;
import com.zdt.common.utils.SpringUtil;
import com.zdt.framework.web.service.TokenService;
import com.zdt.order.domain.OeOrderInfoShopee;
import com.zdt.order.mapper.OeOrderInfoShopeeMapper;
import net.sf.jsqlparser.util.SelectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IOeOrderInfoShopeeListener extends AnalysisEventListener<OeOrderInfoShopee> {

    private OeOrderInfoShopeeMapper oeOrderInfoShopeeMapper;

    private TokenService tokenService;

    /**
     * 构造传bean和构造getbean都行(二选一)
     * @param oeOrderInfoShopeeMapper
     */
    public IOeOrderInfoShopeeListener(OeOrderInfoShopeeMapper oeOrderInfoShopeeMapper){
        this.oeOrderInfoShopeeMapper = oeOrderInfoShopeeMapper;
        tokenService = SpringUtil.getBean(TokenService.class);
    }
//    public IOeOrderInfoShopeeListener(){
//        this.oeOrderInfoShopeeMapper = SpringUtil.getBean(OeOrderInfoShopeeMapper.class);
//    }

    private List<OeOrderInfoShopee> OeOrderInfoShopees = new ArrayList<>();

    @Override
    public void invoke(OeOrderInfoShopee data, AnalysisContext context) {
        OeOrderInfoShopees.add(data);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        LoginUser user = tokenService.getLoginUser(ServletUtils.getRequest());
        OeOrderInfoShopees.stream().forEach(OeOrderInfoShopee->{
            OeOrderInfoShopee.setCreateBy(user.getUser().getUserId());
            OeOrderInfoShopee.setCreateTime(new Date());
        });
        oeOrderInfoShopeeMapper.insertOeOrderInfoShopees(OeOrderInfoShopees);
    }
}
