package com.zdt.common.enums;

/**
 * 数据源
 * 
 * @author zdt
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
