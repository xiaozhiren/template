package com.zdt.common.enums;

/**
 * 操作状态
 * 
 * @author zdt
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
