import request from '@/utils/request'

// 查询订单信息列表
export function listShopeeOrder(query) {
  return request({
    url: '/shopee/shopeeOrder/list',
    method: 'get',
    params: query
  })
}

// 查询订单信息详细
export function getShopeeOrder(oeOrderInfoShopeeId) {
  return request({
    url: '/shopee/shopeeOrder/' + oeOrderInfoShopeeId,
    method: 'get'
  })
}

// 新增订单信息
export function addShopeeOrder(data) {
  return request({
    url: '/shopee/shopeeOrder',
    method: 'post',
    data: data
  })
}

// 修改订单信息
export function updateShopeeOrder(data) {
  return request({
    url: '/shopee/shopeeOrder',
    method: 'put',
    data: data
  })
}

// 删除订单信息
export function delShopeeOrder(oeOrderInfoShopeeId) {
  return request({
    url: '/shopee/shopeeOrder/' + oeOrderInfoShopeeId,
    method: 'delete'
  })
}

// 导出订单信息
export function exportShopeeOrder(query) {
  return request({
    url: '/shopee/shopeeOrder/export',
    method: 'get',
    params: query
  })
}
