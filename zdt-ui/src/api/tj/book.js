import request from '@/utils/request'

// 查询书记管理列表
export function listBook(query) {
  return request({
    url: '/tj/book/list',
    method: 'get',
    params: query
  })
}

// 查询书记管理详细
export function getBook(id) {
  return request({
    url: '/tj/book/' + id,
    method: 'get'
  })
}

// 新增书记管理
export function addBook(data) {
  return request({
    url: '/tj/book',
    method: 'post',
    data: data
  })
}

// 修改书记管理
export function updateBook(data) {
  return request({
    url: '/tj/book',
    method: 'put',
    data: data
  })
}

// 删除书记管理
export function delBook(id) {
  return request({
    url: '/tj/book/' + id,
    method: 'delete'
  })
}

// 导出书记管理
export function exportBook(query) {
  return request({
    url: '/tj/book/export',
    method: 'get',
    params: query
  })
}