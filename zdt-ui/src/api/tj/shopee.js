import request from '@/utils/request'

// 查询订单信息列表
export function listShopee(query) {
  return request({
    url: '/tj/shopee/list',
    method: 'get',
    params: query
  })
}

// 查询订单信息详细
export function getShopee(oeOrderInfoShopeeId) {
  return request({
    url: '/tj/shopee/' + oeOrderInfoShopeeId,
    method: 'get'
  })
}

// 新增订单信息
export function addShopee(data) {
  return request({
    url: '/tj/shopee',
    method: 'post',
    data: data
  })
}

// 修改订单信息
export function updateShopee(data) {
  return request({
    url: '/tj/shopee',
    method: 'put',
    data: data
  })
}

// 删除订单信息
export function delShopee(oeOrderInfoShopeeId) {
  return request({
    url: '/tj/shopee/' + oeOrderInfoShopeeId,
    method: 'delete'
  })
}

// 导出订单信息
export function exportShopee(query) {
  return request({
    url: '/tj/shopee/export',
    method: 'get',
    params: query
  })
}

// 下载虾皮订单模板
export function downloadOrderInfoShopee() {
  return request({
    url: '/common/downloadOrderInfoShopee',
    method: 'get'
  })
}

