import store from '@/store'

export default {

    data() {
        return {
            websock: null,
            time : 1000
        };
    },

    computed:{
      token() {
        return store.getters.token;
      },
    },
    mounted(){
        if(store.getters.token){
            this.initWebSocket()
        }
        
    },
    
    //token发生变化时建立连接
    watch:{
      token: {
        handler(val) {
          if(val){
            this.initWebSocket()
          }
        },
        deep: true
      },
    },
    methods:{
    /**
     * 建立socket连接，
     * */
    initWebSocket() { //初始化weosocket 
      const wsuri = 'ws://192.168.1.96:8081/websocket/'
      let  Authorization = store.getters.token;
      this.websock = new WebSocket(wsuri + Authorization)
      this.websock.onmessage = this.websocketonmessage
      this.websock.onopen = this.websocketonopen
      this.websock.onerror = this.websocketonerror
      this.websock.onclose = this.websocketclose
    },
    websocketonopen() { //连接建立之后执行send方法发送数据
        console.log('连接建立成功')
    },
    /**
     * 连接建立失败,断开连接
     * */
    websocketonerror() {
      console.log('连接建立失败')
      this.websock.onclose()
    },
    websocketonmessage(e) { //数据接收
      const redata = JSON.parse(e.data.substring(0, e.data.length))
      switch (redata.noticeType) {
        case "MESSAGE":
          this.$message.info(redata.msg);
        break;

        case "MESSAGEBOX":
          this.$confirm(redata.msg, '消息', {
            confirmButtonText: '已读',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            let noMvvmData = JSON.parse(JSON.stringify(redata)) 
            noMvvmData.read = true
            this.websocketsend(JSON.stringify(noMvvmData))
          }).catch(() => {
            console.log(JSON.stringify(redata))
            this.websocketsend(JSON.stringify(redata))         
          });
        break;

        case "NOTIFICATION":
          this.$notify({
            title: '收到信息',
            message: redata.msg,
            duration: 0
          });
        break;

        default:
          this.$notify({
            title: '收到信息',
            message: redata,
            duration: 0
          });
        break;
      }

      //提示音
      if(redata.diDong){
        let url="http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=6&text="+ encodeURI("你收到有新消息:"+ redata.msg)
        let n = new Audio(url);
        n.play();
      }
    },
    websocketsend(Data) {//数据发送
      this.websock.send(Data)
    },
    websocketclose(e) {  //关闭
      this.time = this.time*2
      console.log('断开连接', e)
      setTimeout(this.initWebSocket, this.time);
    },
    }
}